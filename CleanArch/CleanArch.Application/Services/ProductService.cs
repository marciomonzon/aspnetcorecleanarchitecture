﻿using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModels;
using CleanArch.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public Task<ProductViewModel> GetById(int? id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProductViewModel>> GetProductsAsync()
        {
            throw new NotImplementedException();
        }

        public void Add(ProductViewModel product)
        {
            throw new NotImplementedException();
        }

        public void Update(ProductViewModel product)
        {
            throw new NotImplementedException();
        }

        public void Delete(int? product)
        {
            throw new NotImplementedException();
        }
    }
}
