﻿using CleanArch.Domain.Entities;

namespace CleanArch.Domain.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
        Task<Product> GetById(int? id);

        void Add(Product product);
        void Update(Product product);
        void Delete(int? id);
    }
}
